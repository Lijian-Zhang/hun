# HUN - High-performance Userspace Network
High-performance Userspace Network are composed of a set of networking open source components, e.g., DPDK, VPP, ODP, OVS, Snort, plus some glue code/scripts.

## Description
HUN aims to provide high-throughput packet processing software stack and solutions to solve customer and partner use case in networking applications. HUN is mainly implemented, validated and deployed on Arm Aarch64 architecture.

The HUN architecture is shown in the following figure:
![High-performance Userspace Network](doc/imgs/UserSpaceDataplane.png "High-performance Userspace Network")

The reference applications/solutions HUN implemented serves multiple purposes,
* Showcase the integration of various components and act as poof of concept all stakeholders
* Allow for performance analysis/optimization with a solution that is close to customers' production deployment
* Provide customers with a out-of-the-box reference for rapid design modeling 

The target platforms to run HUN on, and target verification includes,
* Silicon platforms - server or embedded level CPUs - Functionalities and Performance
* Fast model - CPU IP software simulations - Functionalities
* FPGA Emulator - RTL based CPU IP system emulator - Functionalities and Performance

"Fast model" and "FPGA Emulator" are some pre-silicon simulation/emulation platforms, which could be used to develop and validate software for CPU features in early stage. Running the HUN on these platforms could introduce benefits on,
* External CPU/IP dimensioning according to different customer use-case
* Internal microarchitecture profiling and tuning using networking workload during CPU design phase

## Getting started
...

## Examples
...

## Key features
* Integrates DPDK, VPP and SNORT/Hyper-scan
* Integrates security libraries (OpenSSL, IPSec lib) and hardware offloads
* Provides T-Rex and DPDK-Pktgen traffic generator
* Supports IXIA traffic generator.

## Use cases
* Switching, IPv4/v6 forwarding
* IPSec gateway – integration with OpenSSL, ATG IPSec, offloading to PCIe accelerators
* Next generation firewall – VPP with NAT/ACL, SNORT/Hyperscan for IPS/DPS
* Wireless mid-haul and back-haul transport
* TCP termination and SSL proxy

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

