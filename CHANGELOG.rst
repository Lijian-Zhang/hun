Release Notes
=============


Release 22.06
-----------------

**New Features**

    #. Basic IPv4 L2 switching and L3 routing based on DPDK L3fwd
    
    #. Basic IPv4 L2 switching and L3 routing functionalities based on VPP

**Resolved Issues**

    #. None


Release 22.01
-----------------

**New Features**

    #. None

**Resolved Issues**

    #. None

