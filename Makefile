# SPDX-License-Identifier: Apache-2.0
#
# Copyright(c) 2022, Arm Limited.

.PHONY: all
all:
	@echo "To figure out supplementing required subcommands"

.PHONY: doc
doc:
	cd ./doc && make html

.PHONY: doc-clean
doc-clean:
	cd ./doc && make clean

.PHONY: help
help:
	@echo "Make Targets:"
	@echo " all                  - All targets"
	@echo " doc                  - Build the Sphinx documentation"
	@echo " doc-clean            - Remove the generated files from the Sphinx docs"
