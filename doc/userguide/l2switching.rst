..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: Apache-2.0

L2 Switching
============

This documentation explains in details on how to use the L2 switching
related use cases for end users on Arm platform.

Overview
--------

Bootup
------

Configurations
--------------

Parameters
----------

Commandline References
----------------------