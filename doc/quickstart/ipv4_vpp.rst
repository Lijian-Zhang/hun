..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: Apache-2.0

Quickstart with VPP based IPv4
==============================

This chapter explains how to quickly get started with the VPP based IPv4
routing function.

Set up Environment
------------------


Get Source Code
---------------


Run and Configure Network Function
-----------------------------------


Send Traffic and Validate
-------------------------
