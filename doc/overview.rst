..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: Apache-2.0

Overview
========

The data-plane stack project provides users with some reference
implementations of high performance user space networking functionalities
on various Arm platforms.

This software is mainly focusing on the
:ref:`data_plane`,
as performance is the primary concern of in data-plane implementation on
COTS generic servers, e.g., Ampere Altra, AWS Gravition3, Alibaba Yitian 710.
Arm OSS Networking team focuses on optimizing and providing high performance
data-plane software on Arm platforms, which is the expectation/requirement
of Arm partners and customers also.

This project is providing the networking functionalities based on some
well-known
:ref:`userspace_network` open source project:

* `DPDK <https://www.dpdk.org/>`_:
  acronym of Data Plane Development Kit, mostly runs in userspace.
  It comprises of libraries to achieve high I/O performance and reach high
  packet processing rates, which are some of the most important features in
  the networking area. It supports multivendors and architectures, and runs
  on variety of CPU architectures, e.g., Arm, POWER, x86.
  DPDK was created for the telecom/datacom infrastructure,
  but today, it's used almost everywhere, including the cloud, data centers,
  appliances, containers and more.

* `VPP <https://fd.io/>`_:
  acronym of Vector Packet Processor (VPP) is a fast, scalable layer
  2-4 multi-platform network stack that provides out-of-the-box production
  quality switch/router functionality. It runs in Linux Userspace on multiple
  architectures including ARM, Power and x86 architectures.
  The benefits of this implementation of VPP are its high performance, proven
  technology, its modularity and flexibility, and rich feature set.

* `Snort <https://www.snort.org/>`_:
  is the foremost Open Source Intrusion Prevention System (IPS)
  in the world. Snort IPS uses a series of rules that help define malicious
  network activity and uses those rules to find packets that match against
  them and generates alerts for users.


Implement Consideration
-----------------------

This software aims to provide high-throughput packet processing software
stack and solutions to solve customer and partner use case in networking
applications. There are several considerations from technical perspective
in the implementation process:

* **Integrates DPDK, VPP and SNORT/Hyperscan**

  As mentioned above, the network functions this software provided are based
  some open source project, e.g., DPDK, VPP, Snort3, plus some glue scripts
  to combine them together for a complete solution.

* **Execute on Arm**

  The software is mainly implemented, validated, optimized and deployed on
  Arm Aarch64 architecture and platforms. The target platforms are listed
  in later section.

* **Optimize for Arm architectures**

  Some of above open source projects are well-supported in community,
  including arch-specific compilation, community CI/CD, distro packages,
  arch-specific optimization, etc. While some of the projects are yet to be
  supported.

  What's why the software has developed some scripts to resolve compilation
  issues on Arm platforms, compile project source code with arch-specific
  features, apply optimization patches done by Arm OSS Networking, deploy
  optimal parameters tuned for specific Arm platforms, etc.

* **Integrates security libraries, e.g., OpenSSL, IPSec**

  A group of protocols are supported in established use cases to set up
  encrypted connections with the typical security libraries between devices.
  It helps keep data sent over public networks secure. 

* **Support hardware offloads**

  The solution supports use cases that allow to offload some router features
  onto the underneath hardware. This allows reaching wire speeds when routing
  packets, which simply would not be possible with the CPU. 

* **Provide multiple traffic generator along with solutions**
  
  To verify software functions and integrity, several packet generators are
  supported and provided along with the solution software, e.g.,
  `TRex <https://trex-tgn.cisco.com/>`_,
  `DPDK Pktgen <https://pktgen-dpdk.readthedocs.io/en/latest/>`_,
  `Scapy <https://scapy.net/>`_,
  and IXIA traffic generator.

Purposes
~~~~~~~~

The network functions this software provided serves multiple purposes of,

* Showcase the integration of various components and act as poof of concept
  to all stakeholders.

* Allow for performance analysis/optimization with a solution that is close
  to customers' production deployment.

* Provide customers with a out-of-the-box reference design for rapid design
  modeling. 

to the potential end users of,

* **Arm CPU DV team**

  who will run the software as typical network workload on the verification
  platforms to validate basic CPU logic and new features, analyze CPU behavior
  and bottle-neck, etc.

* **Arm CPU performance analysis expert**

  as software optimization expert, who will run the software on real silicon
  CPUs to benchmark and analyze CPU performance and bottle-neck.

* **Arm marketing team on Networking area**

  who will take the project to showcase and promote Arm availability in
  Networking industry.

* **Arm partners like Marvell, Nvdia, Alibaba**

  who will take this software as verification use cases during their SOC
  verification stage and reference design to develop software for their
  customers.

* **Customer of Arm partners like Cisco, Nokia**

  who will take this software as reference design to develop software for
  their network product and for fast modeling.

* **Network developers who develop network software on Arm platforms**

  who is developing network software, would like to have a quick try with to
  Arm platform, and is looking for some reference software establish
  confidence and accelerate development cycles. 

Target Platforms
~~~~~~~~~~~~~~~~

The target platforms of this software include,

* Silicon platforms - server or embedded level CPUs, mainly focus on network
  functions and performance on Arm CPUs.

* Fast model - CPU IP software simulation platform, mainly focus on verifying
  network functions and CPU behavior analysis in the early IP design stage.

* FPGA Emulator - RTL based CPU IP system emulator, mainly focus on verifying
  network functions, performance benchmarking and CPU behavior analysis in
  the pre-silicon SOC design verification stage.

.. note::

  "Fast model" and "FPGA Emulator" are some pre-silicon simulation/emulation
  platforms, which could be used to develop and validate software for CPU
  features in early stage. Running the software on these platforms could
  introduce benefits on,

	* External CPU/IP dimensioning according to different customer use-case
    
	* Internal microarchitecture profiling and tuning using networking workload
	  during CPU design phase

Network functions
~~~~~~~~~~~~~~~~~

Currently, goal of the software is to establish some typical network
functions as below list, but it will gradually expand to more functions and
features.

The functions below are classified from functionality perspective, instead of
from software component or implementation perspective. For example, IPv4
routing function could be implemented using VPP L3 or DPDK L3 features, and
the fireware function could be provided using VPP ACL/Classifier or Snort3.
Certain use case has a large scope and may be implemened with multiple
software components and instances.

The implementation mechanism of each network function below will be described
in details in later chapter in this documentation.

* **L2 Switching**

  The typical L2 network functions, including MAC learning, bridging,
  cross-connect, flooding, VLAN, etc.

* **IPv4 Routing**

  This is about IPv4 address based packet forwarding, packet header rewrite,
  etc.

* **IPv6 Routing**

  IPv6 is almost identical to IPv4 routing under CIDR. The major difference
  is the addresses are 128-bit IPv6 addresses instead of 32-bit IPv4
  addresses.

* **NAT**

  Network address translation (NAT) is a method of mapping an IP address
  space into another by modifying network address information in the IP
  header of packets while they are in transit across a traffic routing
  device. This software will cover the basic NAT functions, e.g., SNAT,
  DNAT.
  
* **VxLAN**

  VXLAN is an encapsulation protocol that provides data center connectivity
  using tunneling to stretch Layer 2 connections over an underlying Layer 3
  network. In data centers, VXLAN is the most commonly used protocol to
  create overlay networks that sit on top of the physical network, enabling
  the use of virtual networks.
  
* **IPSec**

  IPsec is a framework of techniques used to secure the connection over the
  network communication. The typical encryption/decryption algorithms will 
  be covered in the implementation.

* **Firewall**

  A firewall is a network security device that monitors incoming and outgoing
  network traffic and permits or blocks data packets based on a set of
  security rules. The typical security applications will be demonstrated
  based on some VPP and Snort3 security implementations.

* **TCP termination**

  This function is primarily based on VPP host-stack, but will investigate
  the implementations with some other open source TCP/IP user software.

* **SSL Proxy**

  The SSL proxies control Secure Sockets Layer – SSL traffic -to ensure
  secure transmission of data between a client and a server. The SSL
  proxy is transparent, which means it performs SSL encryption and
  decryption between the client and the server.

* **Wireless Mid-haul or Back-haul**

  Backhaul is better known than fronthaul. It refers to the connections
  between a mobile network and a wired network that backhaul traffic from
  disparate cell sites to a mobile switching telephone office.

.. _data_plane_and_control_plane:

Data plane & Control plane
--------------------------

The terms “control plane” and “data plane” are all about the separation of
responsibilities within a networking system. The two most commonly referenced
compositions in networking are the control plane and the data plane. The
control plane is the part of a network that controls how data is forwarded,
while the data plane is the actual forwarding process.

.. _data_plane:

Data Plane
~~~~~~~~~~

* **Control plane**: refers to the all functions and processes that
  determine which path to use to send the packet or frame. Control
  plane is responsible for populating the routing table, drawing network
  topology, forwarding table and hence enabling the data plane functions.
  **Control plane is the process of learning what we will do before sending
  the packet or frame.**

.. _control_plane:

Control Plane
~~~~~~~~~~~~~

* **Data plane**: refers to all the functions and processes that forward
  packets/frames from one interface to another based on control plane logic.
  Routing table, forwarding table and the routing logic constitute the data
  plane function. Data plane packet goes through the router, and incoming and
  outgoing of frames are done based on control plane logic.
  **Data plane is moving the actual packets based on what we learned from
  control plane**.

.. _userspace_network:

Userspace Network
-----------------

User space network software takes exclusive control of a network adapter,
implements the whole NIC driver and develops packet processing framework
completely in userspace.

There are several primary reasons to move the networking functionalities
from the kernel to user space:

* Reduce the number of context switches required to process packet data, as
  each syscall causes a context switch, which takes up time and resources.

* Reduce the amount of software on the stack. Linux kernel provides
  abstractions, which are designed for general purpose and could be quite
  complicated in implemention and packet processing. Customized
  implementation could remove unnecessary abstractions, simplify the logic,
  and improve performance.

* User space drivers are easier to develop and debug than kernel drivers.
  Developing networking functions and getting it merge in mainline kernel
  would take considerable time and effort. Moreover, the function release
  would be bounded by Linux’s release schedule. Finally, bugs in the source
  code may cause the kernel to crash.

The Userspace networking ecosystems has really matured since some user space
networking projects are open sourced, e.g.,
`DPDK <https://www.dpdk.org/>`_,
`VPP <https://fd.io/>`_,
`Snort <https://www.snort.org/>`_.
A whole ecosystem of technologies developed based on user space network
software has emerged.

