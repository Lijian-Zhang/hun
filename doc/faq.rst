..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: Apache-2.0

FAQ
===

#. Q: Is there any existing open source project with similar goals or with
   which this project may conflict or overlap? If yes, please identify the
   project and explain why we should create the ARM open source project
   anyway?

   A: Yes, there are some open source projects serve similar purpose, e.g.,
   OPNFV. However, compared with the software we would like to create, OPNFV
   has a much larger scope for us to cover. Morever, we would like to simply
   the reference solutions, focus on combining some currently being
   contributed networking projects, e.g., DPDK, VPP, ODP, and provide
   reference solutions aligned with exsiting LoB requirements.

   Inside the project, we will apply some optimizations applicable on Arm
   platform only, to achieve better performance, and those architecture
   specific optimizations will not be likely accepted by upstream community.

#. Q: Could you please provide the project description of functionality or
   purpose?

   A: The network functions this software provided serves multiple purposes
   of,

   #. Showcase the integration of various components and act as poof of
      concept to all stakeholders.

   #. Allow for performance analysis/optimization with a solution that is
      close to customers’ production deployment.

   #. Provide customers with a out-of-the-box reference design for rapid
      design modeling.
